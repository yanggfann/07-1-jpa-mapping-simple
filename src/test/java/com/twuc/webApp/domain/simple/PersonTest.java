package com.twuc.webApp.domain.simple;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;


@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class PersonTest {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private EntityManager em;

    @Test
    void person_true() {
        assertTrue(true);
    }

    @Test
    void test_find_by_id() {
        Person person = new Person(1L, "Fan", "Yang");
        personRepository.save(person);

        em.flush();

        em.clear();

        Optional<Person> byId = personRepository.findById(1L);
        assertThat(byId.isPresent()).isTrue();
        assertThat(byId.get().getFirstName()).isEqualTo("Fan");

    }
}
